     --- GUÍA DE INSTALACIÓN ---

Para utilizar el proyecto starter de ionic 3 no hay más que seguir los siguientes pasos:

1 Clonar el repositorio con git clone https://user@bitbucket.org/fwmovilidad/starter-ionic3.git teniendo en cuenta que donde pone "user" deberemos añadir nuestro nombre de usuario.

2 Abrir el archivo package.json y modificar la url de la dependencia mapfre-framework y añadir nuestro nombre de usuario donde pone "user".

3 Instalar dependencias 
    npm install

4 Si se han seguido correctamente los pasos tendremos un proyecto con el que empezaar a desarrollar nuestra aplicación, con el framework de movilidad integrado.


