import { Component, OnInit } from '@angular/core';
import { Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from 'ng2-translate';

import { HomePage } from '../pages/home/home';
import { MenuOption } from "mapfre-framework";

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  rootPage: any = HomePage;
  menuOptions: MenuOption[];

  constructor(
    public app: App,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public translate: TranslateService
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      this.registerServiceWorker();


      statusBar.styleDefault();
      
      //splashScreen.hide();
      
      translate.setDefaultLang('es');
      translate.use('es');

      console.log('platform ready');

      
      this.requestNotificationPermission();
    });


  }

  

  ngOnInit(): void {

    // Subscribe 
    this.subscribeUser();

    this.menuOptions = [
      {
        option: {
          icon: 'ios-apps-outline',
          text: 'INICIO',
        }
      },
      {
        option: {
          icon: 'ios-car-outline',
          text: 'COCHE',
          subText: 'Otros seguros de coche',
          subIcon: 'add'
        },
        subOptions: [
          {
            option: {
              text: 'Audi A1',
              subText: 'Todo riesgo con franquicia de 250 euros'
            },
            subOptions: [
              {
                option: {
                  text: 'Descargar documentacion'
                }
              },
              {
                option: {
                  text: 'Pagar un recibo'
                }
              },
              {
                option: {
                  text: 'Solicitar asistencia'
                }
              },
              {
                option: {
                  text: 'Dar un parte'
                }
              }
            ]
          },
          {
            option: {
              text: 'Audi A8',
              subText: 'Todo riesgo'
            },
            subOptions: [
              {
                option: {
                  text: 'Descargar documentacion'
                }
              },
              {
                option: {
                  text: 'Pagar un recibo'
                }
              },
              {
                option: {
                  text: 'Solicitar asistencia'
                }
              },
              {
                option: {
                  text: 'Dar un parte'
                }
              }
            ]
          }
        ]
      },
      {
        option: {
          icon: 'ios-home-outline',
          text: 'HOGAR',
          subText: 'Otros seguros de hogar',
          subIcon: 'add'
        },
        subOptions: [
          {
            option: {
              text: 'Casa de la playa',
              subText: 'Familiar'
            },
            subOptions: [
              {
                option: {
                  text: 'Descargar documentacion'
                }
              },
              {
                option: {
                  text: 'Pagar un recibo'
                }
              },
              {
                option: {
                  text: 'Solicitar asistencia'
                }
              },
              {
                option: {
                  text: 'Dar un parte'
                }
              }
            ]
          }
        ]
      },
      {
        option: {
          icon: 'ios-more-outline',
          text: 'OTROS SEGUROS'
        },
        subOptions: [
          {
            option: {
              text: 'Coche',
              icon: 'car'
            }
          },
          {
            option: {
              text: 'Hogar',
              icon: 'home'
            }
          },
          {
            option: {
              text: 'Moto',
              icon: 'bicycle'
            }
          },
          {
            option: {
              text: 'Salud',
              icon: 'flask'
            }
          },
          {
            option: {
              text: 'Decesos',
              icon: 'hand'
            }
          },
          {
            option: {
              text: 'Viaje y ocio',
              icon: 'globe'
            }
          },
          {
            option: {
              text: 'Vida',
              icon: 'heart'
            }
          },
          {
            option: {
              text: 'Otros',
              icon: 'more'
            }
          },
        ],
        type: 2
      },
    ];
  }

  // Request notification permission
  requestNotificationPermission(): void {
    if (Notification.requestPermission) {
        Notification.requestPermission(function(result) {
          console.log("NotificationPermission: ", result);  
                    
        });
    } else {
        console.log("Notificationssupported by this browser.");
    }
  }

  // Method that register the ServiceWorker 
  registerServiceWorker(): void {
    navigator.serviceWorker.register('service-worker.js').then(function(reg) {
      console.log('Service Worker Registered!', reg);
  
      reg.pushManager.getSubscription().then(function(sub) {
        if (sub === null) {
          // Update UI to ask user to register for Push
          console.log('Not subscribed to push service!');          
          
        } else {
          // We have a subscription, update the database
          console.log('Subscription object: ', sub);
        }
      });
    })
     .catch(function(err) {
      console.log('Service Worker registration failed: ', err);
    });
  }

  // Subscribe for notification web 
  subscribeUser(): void {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.ready.then(function(reg) {  
        reg.pushManager.subscribe({
          userVisibleOnly: true
        }).then(function(sub) {
          console.log('Endpoint URL: ', sub.endpoint);
        }).catch(function(e) {
          if (Notification['permission'] === 'denied') {
            console.warn('Permission for notifications was denied');
          } else {
            console.error('Unable to subscribe to push', e);
          }
        });
      })
    }
  }
  
  

  
}

